var express = require('express');
var mysql = require('mysql');
var bodyParser = require('body-parser');
var parseurl = require('parseurl');
var session = require('express-session');
var MySQLStore = require('express-mysql-session')(session);
var fs = require('fs');

var conn = mysql.createConnection({
	host: '127.0.0.1',
	user: 'root',
	password: '@Loverudal2637',
	database: 'dm1'
    });

    conn.connect();


var app = express();

    app.set('views','./views');
    app.set('view engine', 'jade');
    app.use(bodyParser.urlencoded({ extended: false }));


    app.use(session({
    secret: '1234DSFs@adf1234!@#$asd',
    resave: false,
    saveUninitialized: true,
    store:new MySQLStore({
    host:'127.0.0.1',
    port:3306,
    user:'root',
    password:'@Loverudal2637',
    database:'dm1'
    })
    }));

//로그인 후 개인계정 privatCar add   carAdd.jade


app.post('/DM/privateCar/add',function(req,res){
    var carAdd = {
    carBrand : req.body.carBrand,
    carMacroSeries : req.body.carMacroSeries,
    carMicroSeries : req.body.carMicroSeries,
    carName : req.body.carName,
    carNameSeries : req.body.carNameSeries,
    carNumber : req.body.carNumber,
    carColor : req.body.carColor,
    carDistance : req.body.carDistance,
    carAge : req.body.carAge,
    carAgeYear : req.body.carAgeYear,
    carAgeMonth : req.body.carAgeMonth,
    carImport : req.body.carImport,
    carAccident : req.body.carAccident,
    carAccidentCheck : req.body.carAccidentCheck,
    carAfterService : req.body.carAfterService,
    carNewPrice : req.body.carNewPrice,
    carPublicPrice : req.body.carPublicPrice,
    carPrivatePrice : req.body.carPrivatePrice,
    carCashLease : req.body.carCashLease,
    carDescription : req.body.carDescription,
    carStatus : req.body.carStatus,
    allCarList : req.body.allCarList,
    currentUserId : req.session.userId
    };
    var privateCarSql = 'INSERT INTO privateCar SET ?';
        conn.query(privateCarSql, carAdd, function(err, result, fields){
    if(err){
        console.log("DB Error Occurred");
    } else {
        res.redirect('/DM/privateCar');
        };
        });
        });



app.get('/DM/layBprivateCar/add', function(req,res){
    var privateCarSql = 'SELECT * from privateCar';
        conn.query(privateCarSql,function(err, privateCars, fields){
    if(err){    
        console.log("DB Error Occurred1");
        return;
    } else {  
        res.render('layBcarAdd',{privateCars:privateCars});
        };
        });
        });





//로그아웃

app.get('/auth/logout', function(req, res){
    delete req.session.userId;
         res.redirect('/auth/login');
  });



//로그인

app.post('/auth/login', function(req, res){
    var requestUserId=req.body.userId;
    var requestPassword=req.body.password;
    var loginSql = "SELECT * FROM users1 where userId='" + requestUserId + "'";
        conn.query(loginSql, function(err, dbResultList, fields){
    if(err) {
        console.log("DB Error Occurred");
        return;
        }
    
    var dbUserId = dbResultList[0].userId;
    var dbUserPassword = dbResultList[0].password;		
    var dbId = dbResultList[0].id;

    var loginSuccess = (requestUserId == dbUserId && requestPassword == dbUserPassword);

    if(loginSuccess) {
        req.session.userId = dbId;			 
        console.log(dbId);
        res.render('main', {dbResultList:dbResultList});
    } else {
        console.log("login Fail");
        res.send('Who are you? <a href="/auth/login">login</a>');
        }
        });
        });




app.get('/auth/login' , function(req,res){
    res.render('login');
    });





//회원가입

app.post('/auth/register', function(req, res){ 
    var user = {
        area:req.body.area,
        johap:req.body.johap,
        companyName:req.body.companyName,
        position:req.body.position,
        userId:req.body.userId,
        password:req.body.password,
        phoneNumber:req.body.phoneNumber,
        koreanName:req.body.koreanName,
        ssnFront:req.body.ssnFront,
        sex:req.body.sex,
        confirm:req.body.confirm,
        displayName:req.body.koreanName+'_'+req.body.ssnFront
        };
    var sql = 'INSERT INTO users1 SET ?';
        conn.query(sql, user, function(err, results,fields){

    if(err){
        console.log(err);
        res.status(500);
    } else {

        res.redirect('/auth/login');
  
        }
        });
        });





app.get('/auth/register', function(req,res){
    res.render('register');
    });





//companyGroup회원가입

app.post('/auth/companyGroupRegister', function(req, res){ 
    var companyGroup = {
        area:req.body.area,
        johap:req.body.johap,
        companyType:req.body.companyType,
        companyName:req.body.companyName,
        companyPhoneNumber:req.body.companyPhoneNumber,
        companyAddress:req.body.companyAddress,
        directorName:req.body.directorName,
        directorPosition:req.body.directorPosition,
        directorPhoneNumber:req.body.directorPhoneNumber,
        directorUserId:req.body.directorUserId
        };
    var sql = 'INSERT INTO companygroup SET ?';
        conn.query(sql, companyGroup, function(err, results,fields){

    if(err){
        console.log(err);
        res.status(500);
    } else {
        res.redirect('/auth/login');
        }
        });
        });





app.get('/auth/companyGroupRegister', function(req,res){
    res.render('companyGroupRegister');
    });






//첫화면 필요서류 탭

app.get('/auth/needsFile', function(req,res){
    res.render('needsFile');
    });






// //딜러매니저 운영자페이지 masterPage   masterPage.jade


app.get('/DM/masterPage', function(req,res){
    res.render('masterPage');
    });

// //딜러매니저 운영자페이지 유저관리 masterPageUser masterPageUser.jade

app.get(['/DM/masterPage/user'], function(req,res){
    var sql = 'SELECT * FROM users1 WHERE confirm=?';
        conn.query(sql , 'no', function(err, masterPage, fields){
    if(err){
        console.log(err);
        res.status(500).send('Internal Server Error');
    } else {
        res.render('masterPageUser', {masterPage:masterPage});
        }
        });
        });



// //딜러매니저 운영자페이지 유저 Confirm masterPageUserConfirm masterPageUserConfirm.jade

app.post(['/DM/masterPage/user/:id/confirm'], function(req, res){
    var confirm = req.body.confirm;
    var id = req.params.id;
    var sql = 'UPDATE users1 SET confirm=? where id = ?';
        conn.query(sql, [confirm,id], function(err,masterPage, fields){
            console.log(id);
    if(err){
        console.log(err);
        res.status(500).send('Internal Server Error');
    } else {
        res.redirect('/DM/masterPage/user');
        }
        });
        });



app.get(['/DM/masterPage/user/:id/confirm'], function(req, res){
    var id = req.params.id;
   
    var sql = 'SELECT * FROM users1 WHERE id=?';
        conn.query(sql, id, function(err, masterPage, fields){
           
    if(err){
        console.log(err);
        res.status(500).send('Internal Server Error');
    } else {
        res.render('masterPageUserConfirm', {masterPage:masterPage[0]});
        }
        });
        });






//로그인 후



//로그인 후 개인계정 내 정보 myinfo.jade

app.get('/DM/layBmyinfo', function(req,res){
    var currentUserId = req.session.userId;
    var myinfoSql = "SELECT * FROM users1 where id=? ";
        conn.query(myinfoSql, [req.session.userId],function(err,myinfo,fields){
    if(err){
        console.log("DB Error Occurred");
        return;
    } else {
        res.render('layBmyinfo',{myinfo:myinfo[0]});
        }
        });
        });





//로그인 후 개인계정 myinfo edit     myinfo.jade


app.post(['/DM/myinfo/edit'], function(req, res){
    var infoEdit = {
    area : req.body.area,
    johap : req.body.johap,
    companyName : req.body.companyName,
    position : req.body.position,
    password : req.body.password,
    phoneNumber : req.body.phoneNumber,
    sex : req.body.sex
    }
    var sql = 'UPDATE users1 SET ? WHERE id=?';
        conn.query(sql, [infoEdit,req.session.userId] , function(err,myinfo, fields){
    if(err){
        console.log(err);
        res.status(500).send('Internal Server Error');
    } else {
        res.redirect('/DM/myinfo');
        }
        });
        });



app.get(['/DM/myinfo/edit'], function(req, res){
    var myinfoSql = "SELECT * FROM users1 where id = ?";
        conn.query(myinfoSql, [req.session.userId],function(err,myinfo,fields){
    if(err){
        console.log(err);
        res.status(500).send('Internal Server Error');
    } else {
        res.render('myinfoEdit', {myinfo:myinfo[0]});
        }
        });
        });


app.get(['/DM/layBmyinfo/edit'], function(req, res){
    var myinfoSql = "SELECT * FROM users1 where id = ?";
        conn.query(myinfoSql, [req.session.userId],function(err,myinfo,fields){
    if(err){
        console.log(err);
        res.status(500).send('Internal Server Error');
    } else {
        res.render('layBmyinfoEdit', {myinfo:myinfo[0]});
        }
        });
        });




//로그인 후 회원탈퇴 myinfoDelete    myinfoDelete.jade




app.post('/DM/layBmyinfo/delete', function(req, res){
    var sql = 'DELETE FROM users1 where id = ?';
        conn.query(sql, [req.session.userId], function(err,myinfo,fields){
        res.redirect('/DM/layBmyinfo/');
        });
        });


app.get('/DM/layBmyinfo/delete', function(req, res){
    var myinfoSql = "SELECT * FROM users1 where id = ?";
        conn.query(myinfoSql, [req.session.userId],function(err,myinfo,fields){
    if(myinfo.length === 0){
        console.log('There is no record.');
        res.status(500).send('Internal Server Error');
    } else {
        res.render('layBmyinfoDelete', {myinfo:myinfo[0]});
        }
        });
        });







//개인계정 privateCar 볼 수있는 페이지

// app.get(['/DM/privateCar'],function(req,res){
//     var privateCarSql = 'SELECT carId, carName, carStatus FROM privateCar where currentUserId= ? and carStatus = "판매중"';
//         conn.query(privateCarSql,[req.session.userId], function(err,privateCars,fields){
//     var privateCarSellSql = 'SELECT carId, carName, carStatus FROM privateCar where currentUserId= ? and carStatus = "판매완료"';
//         conn.query(privateCarSellSql,[req.session.userId], function(err,privateCarsSell,fields){
//     if(err){
//         console.log("DB Error Occurred");
//         return;
//     } else {
//         res.render('privateCar',{privateCars:privateCars,privateCarsSell:privateCarsSell});
//         };
//         });
//         });
//         });

app.get(['/DM/layBprivateCar'],function(req,res){
    var privateCarSql = 'SELECT * FROM privateCar where currentUserId= ? and carStatus = "판매중"';
        conn.query(privateCarSql,[req.session.userId], function(err,privateCars,fields){
    var privateCarSellSql = 'SELECT * FROM privateCar where currentUserId= ? and carStatus = "판매완료"';
        conn.query(privateCarSellSql,[req.session.userId], function(err,privateCarsSell,fields){
    if(err){
        console.log("DB Error Occurred");
        return;
    } else {
        res.render('layBprivateCar',{privateCars:privateCars,privateCarsSell:privateCarsSell});
        };
        });
        });
        });



// app.get(['/DM/privateCar'],function(req,res){
//     var privateCarSql = 'SELECT carId, carName, carStatus FROM privateCar where currentUserId= ? and carStatus = "판매중"';
//         conn.query(privateCarSql,[req.session.userId], function(err,privateCars,fields){
//     if(err){
//         console.log("DB Error Occurred");
//         return;
//     } else {
//         res.render('privateCar',{privateCars:privateCars});
//         };
//         });
//         });

// app.get(['/DM/privateCar'],function(req,res){
//     var privateCarSellSql = 'SELECT carId, carName, carStatus FROM privateCar where currentUserId= ? and carStatus = "판매완료"';
//         conn.query(privateCarSellSql,[req.session.userId], function(err,privateCarsSell,fields){
    
//     if(err){
//         console.log("DB Error Occurred");
//         return;
//     } else {
//         res.render('privateCar',{privateCarsSell:privateCarsSell});
//         };
//         });
//         });




// app.get(['/DM/privateCar'],function(req,res){
//      var privateCarSql = 'SELECT carId, carName, carStatus FROM privateCar where currentUserId= ? and carStatus = "판매중"';
//         conn.query(privateCarSql,[req.session.userId], function(err,privateCars,fields){
//     if(allCarList='yes'){
//         var carName = req.query.carName;
//      var searchSql = "SELECT * FROM privateCar WHERE carName like '%" + carName + "%'";
//         conn.query(searchSql, function(err,B,fields){
//     if(err){
//         console.log("DB Error Occurred");
//         return;
//     } else {
//         res.render('A',{A:A, B:B});
//         };
//         });
//     } else {
//         res.render('A',{A:A});
//         }
//         });
//         });






//개인계정 privateCar에서 차를 선택해서 수정할 수 있는 페이지



app.get(['/DM/layBprivateCar/:carId'],function(req,res){
    var carId = req.params.carId;
    var privateCarSql = 'SELECT * FROM privateCar WHERE carId = ?';
        conn.query(privateCarSql,[carId],function(err,privateCar,fields){
    if(err){
        console.log("DB Error Occurred");
        return;
    } else {
        res.render('layBprivateCarInfo',{privateCar:privateCar[0]});
        };
        });
        });






//로그인 후 개인계정 privateCar edit     carEdit.jade

app.post(['/DM/privateCar/:carId/edit'], function(req, res){
    var carEdit = {
    carBrand : req.body.carBrand,
    carMacroSeries : req.body.carMacroSeries,
    carMicroSeries : req.body.carMicroSeries,
    carName : req.body.carName,
    carNameSeries : req.body.carNameSeries,
    carNumber : req.body.carNumber,
    carColor : req.body.carColor,
    carDistance : req.body.carDistance,
    carAge : req.body.carAge,
    carAgeYear : req.body.carAgeYear,
    carAgeMonth : req.body.carAgeMonth,
    carImport : req.body.carImport,
    carAccident : req.body.carAccident,
    carAccidentCheck : req.body.carAccidentCheck,
    carAfterService : req.body.carAfterService,
    carNewPrice : req.body.carNewPrice,
    carPublicPrice : req.body.carPublicPrice,
    carPrivatePrice : req.body.carPrivatePrice,
    carCashLease : req.body.carCashLease,
    carDescription : req.body.carDescription,
    carStatus : req.body.carStatus,
    allCarList : req.body.allCarList
    };

       
    var carId = req.params.carId;
    var sql = 'UPDATE privateCar SET ? WHERE carId=?';
        conn.query(sql, [carEdit,carId], function(err,privateCar, fields){
    if(err){
        console.log(err);
        res.status(500).send('Internal Server Error');
    } else {
        res.redirect('/DM/privateCar/'+carId);
        }
        });
        });


app.get(['/DM/privateCar/:carId/edit'], function(req, res){
    var sql = 'SELECT carId,carName FROM privateCar';
        conn.query(sql, function(err, privateCars, fields){
    var carId = req.params.carId;
        if(carId){
    var sql = 'SELECT * FROM privateCar WHERE carId=?';
        conn.query(sql, [carId], function(err, privateCar, fields){
    if(err){
        console.log(err);
        res.status(500).send('Internal Server Error');
    } else {
        res.render('carEdit', {privateCars:privateCars, privateCar:privateCar[0], user:req.user});
        }
        });
    } else {
        console.log('There is no id.');
        res.status(500).send('Internal Server Error');
        }
        });
        });





//로그인 후 개인계정 privateCar delete     carDelete.jade

app.post('/DM/privateCar/:carId/delete', function(req, res){
    var carId = req.params.carId;
    var sql = 'DELETE FROM privateCar WHERE carId=?';
        conn.query(sql, [carId], function(err, privateCar){
        res.redirect('/DM/privateCar/');
        });
        });


app.get('/DM/privateCar/:carId/delete', function(req, res){
    var sql = 'SELECT carId,carName FROM privateCar';
    var carId = req.params.carId;
        conn.query(sql, function(err, privateCars, fields){
        console.log(carId)
    var sql = 'SELECT * FROM privateCar WHERE carId=?';
        conn.query(sql, [carId], function(err, privateCar){
    if(err){
        console.log(err);
        res.status(500).send('Internal Server Error');
    } else {
    if(privateCar.length === 0){
        console.log('There is no record.');
        res.status(500).send('Internal Server Error');
    } else {
        res.render('carDelete', {privateCars:privateCars, privateCar:privateCar[0], user:req.user});
        }
        }
        });
        });
        });

//내 상사 페이지 상사 정보, 인원확인, 차량리스트확인



app.get(['/DM/layBcompanyGroup'],function(req,res){
    var companySql = 'SELECT users1.id, users1.companyName , companyGroup.* FROM users1 INNER JOIN companyGroup On users1.companyName= companyGroup.companyName where users1.id = ?';
        conn.query(companySql,[req.session.userId],function(err,company,fields){
    if(err){
        console.log("DB Error Occurred");
        return;
    } else {
        res.render('layBcompanyGroup',{company:company});
        };
        });
        });



//내 상사 인원 리스트 companyGroupUser.jade

app.get(['/DM/companyGroup/companyGroupUser'],function(req,res){
    var companySql = 'SELECT companyName from users1 WHERE users1.id = ?';
        conn.query(companySql,[req.session.userId], function(err,company,fields){
    if(company){
    var dbcompanyName = company[0].companyName;
    var userSql = 'SELECT * FROM users1 WHERE companyName = ?';
        conn.query(userSql,[dbcompanyName],function(err,users,fields){
    if(err){
        console.log("DB Error Occurred");
        return;
    } else {
        res.render('companyGroupUser',{company:company, users:users});
        };
        });
    } else {
        res.render('companyGroupUser',{company:company});
        }
        });
        });






//내 상사차량 리스트 companyCarList.jade

app.get(['/DM/companyGroup/companyCarList'],function(req,res){
    var companySql = 'SELECT companyName from users1 WHERE users1.id = ?';
        conn.query(companySql,[req.session.userId], function(err,company,fields){
    if(company){
    var dbcompanyName = company[0].companyName;
    var privateCarSql = 'SELECT users1.*, privateCar.* FROM users1 INNER JOIN privateCar ON privateCar.currentUserId = users1.id  where companyName =?';
        conn.query(privateCarSql,[dbcompanyName,req.session.userId],function(err,privateCars,fields){
    if(err){
        console.log("DB Error Occurred");
        return;  
    } else {
        res.render('companyCarList',{company:company, privateCars:privateCars});
        };
        });
    } else {
        res.render('companyCarList',{company:company});
        }
        });
        });


//내 그룹  myGroup


app.get(['/DM/myGroup'],function(req,res){
    var groupSql = 'select * from myGroup where currentUserId = ?';
        conn.query(groupSql,[req.session.userId],function(err,group,fields){
    if(err){
        console.log("DB Error Occurred");
        return;
    } else {
        res.render('myGroup',{group:group});
        };
        });
        });


//myGroup 새그룹 생성

app.post('/DM/myGroup/myGroupRegister', function(req, res){ 
    var myGroup = {
        groupName : req.body.groupName,
        currentUserId : req.session.userId
    };
    var sql = 'INSERT INTO myGroup SET ?';
        conn.query(sql, myGroup, function(err, results,fields){
    if(err){
        console.log(err);
        res.status(500);
    } else {
        res.redirect('/DM/myGroup');
        }
        });
        });


app.get('/DM/myGroup/myGroupRegister', function(req,res){
    res.render('myGroupRegister');
    });






//내그룹 삭제 myGroupDelete     myGroupDelete.jade

app.post('/DM/myGroup/:groupId/myGroupDelete', function(req, res){
    var groupId = req.params.groupId;
    var sql = 'DELETE FROM myGroup WHERE groupId = ?';
        conn.query(sql, [groupId], function(err, myGroup){
            console.log(groupId)
        res.redirect('/DM/myGroup/');
        });
        });


app.get('/DM/myGroup/:groupId/myGroupDelete', function(req, res){
    var groupId = req.params.groupId;
    var sql = 'SELECT * FROM myGroup WHERE groupId = ?';
        conn.query(sql, [groupId], function(err, myGroup, fields){
    if(err){
        console.log(err);
        res.status(500).send('Internal Server Error');
    } else {
        res.render('myGroupDelete', {myGroup:myGroup[0]});
        }
        });
        });
   






//그룹 검색창 myGroupSearch


app.get(['/DM/myGroup/myGroupSearch'],function(req,res){
    var groupName = req.query.groupName;
    var searchSql = "SELECT * FROM myGroup WHERE groupName like '%" + groupName + "%'";
        conn.query(searchSql, function(err,myGroup,fields){
    if(err){
        console.log("DB Error Occurred");
        return;
    } else {
        res.render('myGroupSearch',{myGroup:myGroup});
        };
        });
        });




//내그룹 신청 myGroupReq


app.post('/DM/myGroup/myGroupSearch/:groupId', function(req, res){
    var groupId = req.params.groupId;
    var userId = req.session.userId;
    var searchSql = 'INSERT INTO myGroupUser(groupId,userId) VALUES(?,?)';
        conn.query(searchSql, [groupId,userId], function(err,myGroup,fields){
            console.log(groupId, userId);
        res.redirect('/DM/myGroup/');
        });
        });


app.get('/DM/myGroup/myGroupSearch/:groupId', function(req, res){
    var groupId = req.params.groupId;
    var sql = 'SELECT * FROM myGroup WHERE groupId=?';
        conn.query(sql, groupId, function(err, myGroup){
    if(err){
        console.log(err);
        res.status(500).send('Internal Server Error');
    } else {
        res.render('myGroupReq', {myGroup:myGroup[0]});
        }
        });
        });




//내 그룹 인원리스트 myGroupUser


app.get('/DM/myGroup/:groupId/myGroupUser',function(req,res){
    var groupId = req.params.groupId;
    var groupSql = 'SELECT myGroup.groupId , myGroupUser.groupId FROM myGroup INNER JOIN myGroupUser ON userId=?';
        conn.query(groupSql,[req.session.userId], function(err,group,fields){
    if(group){
    var userSql = 'select myGroupUser.* , users1.* from myGroupUser inner join users1 on myGroupUser.userId = users1.id where myGroupUser.groupId = ? and myGroupUser.groupConfirm = ?';
        conn.query(userSql,[groupId,'yes'],function(err,users,fields){
    if(err){
        console.log("DB Error Occurred");
        return;
    } else {
        res.render('myGroupUser',{group:group, users:users});
        };
        });
    } else {
        res.render('myGroupUser',{group:group});
        }
        });
        });




//내그룹에서 삭제시키기 myGroupUserDelete      myGroupUserDelete.jade  

app.post('/DM/myGroup/:groupUserId/delete', function(req, res){
    var groupUserId = req.params.groupUserId;
    var sql = 'DELETE FROM myGroupUser WHERE groupUserId=?';
        conn.query(sql, groupUserId, function(err, myGroup){
        res.redirect('/DM/myGroup/');
        });
        });


app.get('/DM/myGroup/:groupUserId/delete', function(req, res){
    var groupUserId = req.params.groupUserId;
    var sql = 'SELECT * FROM myGroupUser WHERE groupUserId=?';
        conn.query(sql, groupUserId, function(err, myGroup){
    if(err){
        console.log(err);
        res.status(500).send('Internal Server Error');
    } else {
    if(myGroup.length === 0){
        console.log('There is no record.');
        res.status(500).send('Internal Server Error');
    } else {
        res.render('myGroupUserDelete', {myGroup:myGroup[0]});
        }
        }
        });
        });









//내그룹 신청확인 myGroupReq


app.get('/DM/myGroup/:groupId/myGroupUserReq', function(req, res){
    var groupId = req.params.groupId;
    var sql = 'SELECT myGroupUser.*, users1.* FROM myGroupUser INNER JOIN users1 ON myGroupUser.userId = users1.id where groupConfirm = ? and groupId = ? '
        conn.query(sql,['no', groupId], function(err, myGroup){
            console.log(groupId);
    if(err){
        console.log(err);
        res.status(500).send('Internal Server Error');
    } else {
        res.render('myGroupUserReq', {myGroup:myGroup});
        }
        });
        });






//내그룹 신청확인 yes/no  myGroupReqConfirm/:groupId

app.post('/DM/myGroup/:groupId/myGroupUserReq/:userId/myGroupReqConfirm', function(req, res){
    var groupId = req.params.groupId;
    var userId = req.params.userId;
    var groupConfirm = req.body.groupConfirm;
    var searchSql = 'UPDATE myGroupUser SET groupConfirm = ? where groupId =? and userId =?';
    conn.query(searchSql, [groupConfirm, groupId, userId], function(err,myGroup,fields){
        if(err){
            console.log(err);
            res.status(500).send('Internal Server Error');
        } else {
            res.redirect('/DM/myGroup/');
        }
    });
});



app.get('/DM/myGroup/:groupId/myGroupUserReq/:userId/myGroupReqConfirm', function(req, res){
    var groupId = req.params.groupId;
    var userId = req.params.userId;
    var sql = 'SELECT myGroupUser.*, users1.* FROM myGroupUser INNER JOIN users1 ON myGroupUser.userId = users1.id where groupConfirm = ?'
    
    conn.query(sql,'no', function(err, myGroup){
                        
      if(err){
           console.log(err);
            res.status(500).send('Internal Server Error');
        } else {
            console.log(myGroup[0])
            res.render('myGroupReqConfirm', {myGroup:myGroup[0]});
        }
    });
});




//내가 가입된 그룹

app.get('/DM/myGroup/myGroupList', function(req, res){
    var sql = 'SELECT myGroup.*, myGroupUser.* FROM myGroup INNER JOIN myGroupUser ON myGroup.groupId = myGroupUser.groupId where userId = ? and groupconfirm =?'
        conn.query(sql, [req.session.userId,'yes'], function(err, myGroup){
    if(err){
        console.log(err);
        res.status(500).send('Internal Server Error');
    } else {
        res.render('myGroupList', {myGroup:myGroup});
        }
        });
        });




//내가 가입된 그룹탈퇴 myGroup/myGroupListDelete     myGroupListDelete.jade

app.post('/DM/myGroup/:groupId/myGroupListDelete/:userId', function(req, res){
    var groupId = req.params.groupId;
    var userId = req.params.userId;
    var sql = 'DELETE FROM myGroupUser WHERE userId=? and groupId = ? ';
        conn.query(sql, [req.session.userId,groupId], function(err, myGroup){
        res.redirect('/DM/myGroup/myGroupList');
        });
        });


app.get('/DM/myGroup/:groupId/myGroupListDelete/:userId', function(req, res){
    var groupId = req.query.groupId;
    var userId = req.query.userId
    var sql = 'SELECT * FROM myGroupUser WHERE userId=? and groupId = ?';
        conn.query(sql, [req.session.userId,groupId], function(err, myGroup){
    if(err){
        console.log(err);
        res.status(500).send('Internal Server Error');
    } else {
        res.render('myGroupListDelete', {myGroup:myGroup});
        console.log(myGroup);
        }
        });
        });
    










//찜한 차 카트보는 페이지 myCart      myCart.jade  



app.get(['/DM/myCart','/DM/myCart/:carId'],function(req,res){
    var carId = req.params.carId;
    var myCartSql = 'SELECT myCart.currentUserId, privateCar.* FROM myCart INNER JOIN privateCar ON myCart.carId=privateCar.carId where myCart.currentUserId=?';
        conn.query(myCartSql,[req.session.userId],function(err,myCart,fields){
    if(err){
        console.log("DB Error Occurred");
        return;
    } else {
        res.render('myCart',{myCart:myCart});
        };
        });
        });

app.get(['/DM/layBmyCart','/DM/layBmyCart/:carId'],function(req,res){
    var carId = req.params.carId;
    var myCartSql = 'SELECT myCart.currentUserId, privateCar.* FROM myCart INNER JOIN privateCar ON myCart.carId=privateCar.carId where myCart.currentUserId=?';
        conn.query(myCartSql,[req.session.userId],function(err,myCart,fields){
    if(err){
        console.log("DB Error Occurred");
        return;
    } else {
        res.render('layBmyCart',{myCart:myCart});
        };
        });
        });



//찜한 차 카트삭제 페이지 myCartDelete      myCartDelete.jade  

app.post('/DM/myCart/:carId/delete', function(req, res){
    var carId = req.params.carId;
    var sql = 'DELETE FROM myCart WHERE carId=?';
        conn.query(sql, [carId], function(err, myCart){
        res.redirect('/DM/myCart/');
        });
        });


app.get('/DM/myCart/:carId/delete', function(req, res){
    var carId = req.params.carId;
    var sql = 'SELECT carId,carName FROM myCart';
        conn.query(sql, function(err, myCarts, fields){
    var sql = 'SELECT * FROM myCart WHERE carId=?';
        conn.query(sql, [carId], function(err, myCart){
    if(err){
        console.log(err);
        res.status(500).send('Internal Server Error');
    } else {
    if(myCart.length === 0){
        console.log('There is no record.');
        res.status(500).send('Internal Server Error');
    } else {
        res.render('myCartDelete', {myCarts:myCarts, myCart:myCart[0]});
        }
        }
        });
        });
        });






//userInfo 사용자정보 보는 페이지 

app.get(['/DM/userInfo'],function(req,res){
    var userInfoSql = 'SELECT * FROM users1';
        conn.query(userInfoSql,function(err,userInfo,fields){
    if(err){
        console.log("DB Error Occurred");
        return;
    } else {
        res.render('userInfo',{userInfo:userInfo});
        };
        });
    });
    
app.get(['/DM/layBuserInfo'],function(req,res){
    var userInfoSql = 'SELECT * FROM users1';
        conn.query(userInfoSql,function(err,userInfo,fields){
    if(err){
        console.log("DB Error Occurred");
        return;
    } else {
        res.render('layBuserInfo',{userInfo:userInfo});
        };
        });
        });    

//사용자 검색창 userInfoSearch


app.get(['/DM/userInfo/userInfoSearch'],function(req,res){
    var displayName = req.query.displayName;
    var searchSql = "SELECT * FROM users1 WHERE displayName like '%" + displayName + "%'";
        conn.query(searchSql, function(err,userInfo,fields){
    if(err){
        console.log("DB Error Occurred");
        return;
    } else {
        res.render('userInfoSearch',{userInfo:userInfo});
        };
        });
        });





//userInfo에서 차고를 눌렀을 때 차 리스트를 보여줌 (allCarList에 yes가 되어있는 것만)

app.get(['/DM/userInfo/privateCarList','/DM/userInfo/privateCarList/:id'],function(req,res){
    var privateCarSql = 'SELECT * FROM privateCar where allCarList= ?';
        conn.query(privateCarSql,["yes"], function(err,privateCars,fields){
    var id = req.params.id;
    if(id){
        console.log(id);
    var allCarListSql = 'SELECT users1.id , privateCar.* FROM users1 INNER JOIN privateCar ON privateCar.currentuserId = users1.id where id= ?';
        conn.query(allCarListSql,[id],function(err,allCarList,fields){
    if(err){
        console.log("DB Error Occurred");
        return;
    } else {
        res.render('privateCarList',{privateCars:privateCars, allCarList:allCarList});
        };
        });
    } else {
        res.render('privateCarList',{privateCars:privateCars});
        }
        });
        });





//오른쪽 위 Tab


//allCarList에 yes로 되어있어 모든 이가 볼 수 있는 allCarList 페이지

app.get(['/DM/allCarList'],function(req,res){
    var privateCarSql = 'SELECT * FROM privateCar where allCarList= ?';
        conn.query(privateCarSql,["yes"], function(err,privateCars,fields){
    if(err){
        console.log("DB Error Occurred");
        return;
    } else {
        res.render('allCarList',{privateCars:privateCars});
        }
        });
        });





//allCarListSearch 검색창 (yes한것만 나와야함 위와 연결 필수)

app.get(['/DM/allCarListSearch'],function(req,res){
    var carName = req.query.carName;
    var searchSql = "SELECT * FROM privateCar WHERE carName like '%" + carName + "%'";
        conn.query(searchSql, function(err,privateCars,fields){
    if(err){
        console.log("DB Error Occurred");
        return;
    } else {
        res.render('allCarListSearch',{privateCars:privateCars});
        };
        });
        });









//찜한 차 카트넣는 페이지 allCarListCartAdd     allCarListCartAdd.jade  submit누르면 DB에는 저장되는데 페이지이동이 안됨
app.post(['/DM/allCarList/:carId/cartAdd'],function(req,res){
    var carId = req.params.carId;
    var myCartSql = 'SELECT carId FROM myCart where currentUserId = ?';
        conn.query(myCartSql,[req.session.userId] ,function(err,myCarts,fields){
    if(carId){
    var myCartSql = 'INSERT INTO myCart(currentUserId,carId) VALUES(?,?)';
        conn.query(myCartSql, [req.session.userId, carId], function(err, myCart, fields){
    if(err){
        console.log("DB Error Occurred");
    } else {
        res.redirect('/DM/allCarList');
        return;
        };
        });
        }
        });
        });


app.get(['/DM/allCarList/:carId/cartAdd'],function(req,res){
    var carId = req.params.carId;
    var myCartSql = 'SELECT carId FROM privateCar where carId = ?';
        conn.query(myCartSql,[carId], function(err,myCart,fields){
    if(err){
        console.log("DB Error Occurred");
        return;
    } else {
        res.render('allCarListCartAdd',{myCart:myCart[0]});
        return;
        };
        });   
        });
 

//allCarList에서 상세보기 누르면 나오는 페이지 allCarListInfo
app.get(['/DM/allCarList/:carId/allCarListInfo'],function(req,res){
    var carId = req.params.carId;
    var allCarListSql = 'SELECT * FROM privateCar WHERE carId = ?';
        conn.query(allCarListSql,[carId],function(err,allCarList,fields){
    if(carId){
    var myInfoSql = 'SELECT privateCar.currentUserId, users1.* FROM privateCar INNER JOIN users1 ON privateCar.currentuserId = users1.id where carId = ?';
        conn.query(myInfoSql, [carId], function(err, myInfo, fields){        
    if(err){
        console.log("DB Error Occurred");
        return;
    } else {
        res.render('allCarListInfo',{myInfo:myInfo[0], allCarList:allCarList[0]});
        };
        });
        };
        });
});


//리스사 정보 leaseCompany leaseCompany.jade



app.get('/DM/leaseCompany', function(req,res){
    res.render('leaseCompany');
});


app.get('/DM/layBleaseCompany', function(req,res){
    res.render('layBleaseCompany');
    });


//DM칼럼 게시판 boardDM


app.get('/DM/boardDM/',function(req,res){
    res.redirect('/DM/boardDM/list/1');
    });


app.get('/DM/layBboardDM/',function(req,res){
    res.redirect('/DM/layBboardDM/list/1');
    });



//DM칼럼 게시판 boardDMList   boardDMList.jade

app.get('/DM/boardDM/list/:page',function(req,res){
    var forSelectListSql = "SELECT boardId, displayName, title, date_format(modidate,'%Y-%m-%d %H:%i:%s') modidate,hit FROM boardDM";
        conn.query(forSelectListSql,function(err,rows){
    if(err){
        console.log("DB Error Occurred");
    } else { 
        res.render('boardDMList',{rows:rows});
        }
        });
        });


app.get('/DM/layBboardDM/list/:page',function(req,res){
    var forSelectListSql = "SELECT boardId, displayName, title, date_format(modidate,'%Y-%m-%d %H:%i:%s') modidate,hit FROM boardDM";
        conn.query(forSelectListSql,function(err,rows){
    if(err){
        console.log("DB Error Occurred");
    } else { 
        res.render('layBboardDMList',{rows:rows});
        }
        });
        });


//DM칼럼 게시판 boardDMWrite   boardDMWrite.jade

app.post('/DM/boardDM/write', function(req,res,next){
    var boardDM = {
    displayName : req.body.displayName,
    title : req.body.title,
    content : req.body.content,
    password : req.body.password
    };
    
    var forInsertBoardSql = "INSERT INTO boardDM SET ?)";
        conn.query(forInsertBoardSql, boardDM, function(err,rows){
    if(err){
        console.log("DB Error Occurred");
    } else { 
        res.redirect('/DM/boardDM');
        }
        });
        });



app.get('/DM/boardDM/write', function(req,res,next){
    res.render('boardDMWrite');
    });


//DM칼럼 게시판 boardDMRead   boardDMRead.jade 조회 및 수정

app.get('/DM/boardDM/read/:boardId',function(req,res,next){
    var boardId = req.params.boardId;
    var sql = "SELECT boardId, displayName, title, content, date_format(modidate,'%Y-%m-%d %H:%i:%s') modidate, hit FROM boardDM where boardId=?";
        conn.query(sql,[boardId], function(err,row){
    if(err){
        console.log("DB Error Occurred");
    } else {
        res.render('boardDMRead', {row:row[0]});
        }    
        });
        });



app.get('/DM/layBboardDM/read/:boardId',function(req,res,next){
    var boardId = req.params.boardId;
    var sql = "SELECT boardId, displayName, title, content, date_format(modidate,'%Y-%m-%d %H:%i:%s') modidate, hit FROM boardDM where boardId=?";
        conn.query(sql,[boardId], function(err,row){
    if(err){
        console.log("DB Error Occurred");
    } else {
        res.render('layBboardDMRead', {row:row[0]});
        }    
        });
        });

//DM칼럼 게시판 boardDMModify   boardDMModify.jade 조회 및 수정
app.post('/DM/boardDM/modify/:boardId',function(req,res,next){
    var boardId = req.params.boardId;
    var displayName = req.body.displayName;
    var title = req.body.title;
    var content = req.body.content;
    var password = req.body.password;

    var sql = "UPDATE boardDM SET displayName=? , title=?, content=?, regdate=now() where boardId=? and password=?";
        conn.query(sql,[displayName,title,content,boardId,password],function(err,row){
    if(err){
        console.error("글 수정 중 에러 발생")
    } else {
        res.redirect('/DM/boardDM/read/',{row:row});
        }
        });
        });



app.get('/DM/boardDM/modify/:boardId',function(req,res,next){
    var boardId = req.params.boardId;
    var sql = "SELECT boardId, displayName, title, content, date_format(modidate,'%Y-%m-%d %H:%i:%s') modidate, hit FROM boardDM WHERE boardId=?";
        conn.query(sql, [boardId], function(err,row){
    if(err){
        console.log("DB Error Occurred");
    } else {
        res.render('boardDMModify', {row:row[0]});
        }
        });
        });


//리스 이자율 계산기


app.get('/DM/leaseInterestRate',function(req,res){
    var sql = "SELECT * from leaseInterestRate";
        conn.query(sql, function(err,rate){
    if(err){
        console.log("DB Error Occurred");
    } else {
        res.render('leaseInterestRate', {rate:rate});
        }
        });
        });



app.get('/DM/layBleaseInterestRate',function(req,res){
    var sql = "SELECT * from leaseInterestRate";
        conn.query(sql, function(err,rate){
    if(err){
        console.log("DB Error Occurred");
    } else {
        res.render('layBleaseInterestRate', {rate:rate});
        }
        });
        });



//리스 이자율계산 leaseInterestRateInsert 내역 add하는 부분


app.post('/DM/leaseInterestRate/leaseInterestRateInsert',function(req,res){
    var interest = {
    carName : req.body.carName,
    carNumber : req.body.carNumber,
    leaseCompany : req.body.leaseCompany,
    leaseForm : req.body.leaseForm,
    leaseFixedDate : req.body.leaseFixedDate,
    allInning : req.body.allInning,
    nowInning : req.body.nowInning,
    residualInning : req.body.allInning-req.body.nowInning,
    leaseFee : req.body.leaseFee,
    prePaid : req.body.prePaid,
    nonPrincipal : req.body.nonPrincipal,
    deposit : req.body.deposit,
    residualValue : req.body.residualValue,
    purePrincipal : req.body.nonPrincipal-req.body.deposit,
    veryPurePrincipal : req.body.nonPrincipal-req.body.deposit-req.body.prePaid*(req.body.allInning-req.body.nowInning),
    repayment : req.body.deposit-req.body.residualValue,
    interestRate : [req.body.leaseFee-(req.body.nonPrincipal2-req.body.nonPrincipal3)]*12/req.body.nonPrincipal2,
    allInterest : req.body.leaseFee*(req.body.allInning-req.body.nowInning)-(req.body.nonPrincipal-req.body.residualValue),
    residualPrePaid : req.body.prePaid*(req.body.allInning-req.body.nowInning),
    conditionChange : req.body.conditionChange,
    nonPrincipal2 : req.body.nonPrincipal2,
    nonPrincipal3 : req.body.nonPrincipal3,
    nonPrincipal4 : req.body.nonPrincipal4,
    nonPrincipal5 : req.body.nonPrincipal5
    };
    var interestSql = 'INSERT INTO leaseInterestRate SET ?';
        conn.query(interestSql,interest, function(err, rate, fields){
    if(err){
        console.log("DB Error Occurred");
    } else {
        res.redirect('/DM/leaseInterestRate/');
        };
        });
        });
 

app.get('/DM/leaseInterestRate/leaseInterestRateInsert', function(req,res){
        res.render('leaseInterestRateInsert');
        });
       





app.post('/DM/layBleaseInterestRate/layBleaseInterestRateInsert',function(req,res){
    var interest = {
    carName : req.body.carName,
    carNumber : req.body.carNumber,
    leaseCompany : req.body.leaseCompany,
    leaseForm : req.body.leaseForm,
    leaseFixedDate : req.body.leaseFixedDate,
    allInning : req.body.allInning,
    nowInning : req.body.nowInning,
    residualInning : req.body.allInning-req.body.nowInning,
    leaseFee : req.body.leaseFee,
    prePaid : req.body.prePaid,
    nonPrincipal : req.body.nonPrincipal,
    deposit : req.body.deposit,
    residualValue : req.body.residualValue,
    purePrincipal : req.body.nonPrincipal-req.body.deposit,
    veryPurePrincipal : req.body.nonPrincipal-req.body.deposit-req.body.prePaid*(req.body.allInning-req.body.nowInning),
    repayment : req.body.deposit-req.body.residualValue,
    interestRate : [req.body.leaseFee-(req.body.nonPrincipal2-req.body.nonPrincipal3)]*12/req.body.nonPrincipal2,
    allInterest : req.body.leaseFee*(req.body.allInning-req.body.nowInning)-(req.body.nonPrincipal-req.body.residualValue),
    residualPrePaid : req.body.prePaid*(req.body.allInning-req.body.nowInning),
    conditionChange : req.body.conditionChange,
    nonPrincipal2 : req.body.nonPrincipal2,
    nonPrincipal3 : req.body.nonPrincipal3,
    nonPrincipal4 : req.body.nonPrincipal4,
    nonPrincipal5 : req.body.nonPrincipal5
    };
    var interestSql = 'INSERT INTO leaseInterestRate SET ?';
        conn.query(interestSql,interest, function(err, row, fields){
    if(err){
        console.log("DB Error Occurred");
        return;
    } else {
        res.render('layBleaseInterestRateResult',{row:interest});
        };
        });
        });
 

app.get('/DM/layBleaseInterestRate/layBleaseInterestRateInsert', function(req,res){
        res.render('layBleaseInterestRateInsert');
        });
       





//리스이자율 설명서

app.get('/DM/layBleaseInterestRateManual', function(req,res){
        res.render('layBleaseInterestRateManual');
        });




//리스이자율 설명서 개별 설명


app.get('/DM/layBleaseInterestRateManual/financeLeaseNormal',function(req,res){
    fs.readFile('./images/financeLeaseNormal.png', function(error,data){
            res.statuscode =200;
            res.setHeader('Content-Type','image/png');
            res.end(data);
    });
});

app.get('/DM/layBleaseInterestRateManual/operatingLeasePrepaid',function(req,res){
    fs.readFile('./images/operatingLeasePrepaid.png', function(error,data){
            res.statuscode =200;
            res.setHeader('Content-Type','image/png');
            res.end(data);
    });
});

app.get('/DM/layBleaseInterestRateManual/operatingLeaseCarTax',function(req,res){
    fs.readFile('./images/operatingLeaseCarTax.png', function(error,data){
            res.statuscode =200;
            res.setHeader('Content-Type','image/png');
            res.end(data);
    });
});

app.get('/DM/layBleaseInterestRateManual/postponementLeaseNormal',function(req,res){
    fs.readFile('./images/postponementLeaseNormal.png', function(error,data){
            res.statuscode =200;
            res.setHeader('Content-Type','image/png');
            res.end(data);
    });
});










//리스이자율 차 리스트
app.get('/DM/leaseInterestRate/:id', function(req,res){
    var id = req.params.id;
     var sql = "SELECT * from leaseInterestRate where leaseId = ?";
        conn.query(sql, id, function(err,rate){
    if(err){
        console.log("DB Error Occurred");
    } else {
        res.render('leaseInterestRateCar',{rate:rate}); 
    };
    });
    });


// //실험실

// app.get(['/DM/A'],function(req,res){
//     var privateCarSql = 'SELECT * FROM privateCar where allCarList= ?';
//         conn.query(privateCarSql,["yes"], function(err,A,fields){
//     if(err){
//         console.log("DB Error Occurred");
//         return;
//     } else {
//         res.render('A',{A:A});
//         }
//         });
//         });





// //A 검색창 (yes한것만 나와야함 위와 연결 필수)

// app.get(['/DM/A'],function(req,res){
//     var carName = req.query.carName;
//     var searchSql = "SELECT * FROM privateCar WHERE carName like '%" + carName + "%'";
//         conn.query(searchSql, function(err,B,fields){
//     if(err){
//         console.log("DB Error Occurred");
//         return;
//     } else {
//         res.render('A',{B:B});
//         };
//         });
//         });




// app.get('/DM/A',function(req,res){
//      var privateCarSql = 'SELECT * FROM privateCar where allCarList= ?';
//         conn.query(privateCarSql,["yes"], function(err,A,fields){
//     if(allCarList='yes'){
//         var carName = req.query.carName;
//      var searchSql = "SELECT * FROM privateCar WHERE carName like '%" + carName + "%'";
//         conn.query(searchSql, function(err,B,fields){
//     if(err){
//         console.log("DB Error Occurred");
//         return;
//     } else {
//         res.render('A',{A:A, B:B});
//         };
//         });
//     } else {
//         res.render('A',{A:A});
//         }
//         });
//         });




// app.get('/DM/A',function(req,res){
//      var page = req.query.page;
//      var perPage = req.query.perPage;
//      var dbStart = (page-1)*perPage+1;
//      var dbEnd = page*perPage;
//      var pageMinusFive = page-5;
//      var pagePlusFive = page+5;
//      var i=1;
//      var privateCarSql = 'select * from privateCar where carId between ? and ?';
//      conn.query(privateCarSql,[dbStart,dbEnd], function(err,A,fields){
//     console.log(page);
//     if(err){
//         console.log("DB Error Occurred");
//         return;
//     } else {
//         res.render('A',{A:A, dbStart:dbStart, dbEnd:dbEnd, pageMinusFive:pageMinusFive,pagePlusFive:pagePlusFive });
//         }
//         });
//         });



app.get('/DM/A',function(req,res){
     var page = req.query.page;
     var perPage = req.query.perPage;
     var nowPage = (page-0);
     var nextPage = nowPage+5;
     var prevPage = nowPage-5;
     var privateCarSql = 'select * from privateCar where carId order by carId limit 5 offset ?';
     conn.query(privateCarSql,nowPage, function(err,A,fields){
    if(err){
        console.log("DB Error Occurred");
        return;
    } else {
        res.render('A',{A:A, nowPage:nowPage, nextPage:nextPage, prevPage:prevPage });
        }
        });
        });


app.get('/DM/A/search',function(req,res){
     
    var carName = req.query.carName;
    var searchSql = "SELECT * FROM privateCar WHERE carName like '%" + carName + "%'";
        conn.query(searchSql, function(err,B,fields){
    if(err){
        console.log("DB Error Occurred");
        return;
    } else {
        res.render('ASearch',{ B:B});
        };
        });
        });


app.get('/DM/A/search',function(req,res){
     var page = req.query.page;
     var perPage = req.query.perPage;
     var nowPage = (page-0);
     var nextPage = nowPage+5;
     var prevPage = nowPage-5;
     var privateCarSql = 'select * from privateCar where carId order by carId limit 5 offset ?';
     conn.query(privateCarSql,nowPage, function(err,A,fields){
    if(err){
        console.log("DB Error Occurred");
        return;
    } else {
        res.render('ASearch',{A:A, nowPage:nowPage, nextPage:nextPage, prevPage:prevPage });
        }
        });
        });




    // if(carName){
    // var page = req.query.page;
    // var perPage = req.query.perPage;
    // var nowPage = (page-0);
    // var nextPage = nowPage+5;
    // var prevPage = nowPage-5;
    // var privateCarSql = 'select * from privateCar where carId order by carId limit 5 offset ?';
    //     conn.query(privateCarSql,nowPage, function(err,A,fields){
    // if(err){
    //     console.log("DB Error Occurred");
    //     return;
    // } else {
    //     res.render('ASearch',{A:A, B:B, nowPage:nowPage, nextPage:nextPage, prevPage:prevPage});
    //     };
    //     });
    // } else {
    //     res.render('ASearch',{B:B});
    //     };
    //     });
    //     });    










// app.get('/DM/C', function(req,res){
//         res.render('C');
//     });
    


// app.get(['/DM/C'],function(req,res){
//     var privateCarSql = 'SELECT carId, carName, carStatus FROM privateCar where currentUserId= ? and carStatus = "판매중"';
//         conn.query(privateCarSql,[req.session.userId], function(err,privateCars,fields){
//     if(err){
//         console.log("DB Error Occurred");
//         return;
//     } else {
//         res.render('C',{privateCars:privateCars});
//         };
//         });
//         });

// app.get(['/DM/C'],function(req,res){
//     var privateCarSellSql = 'SELECT carId, carName, carStatus FROM privateCar where currentUserId= ? and carStatus = "판매완료"';
//         conn.query(privateCarSellSql,[req.session.userId], function(err,privateCarsSell,fields){
    
//     if(err){
//         console.log("DB Error Occurred");
//         return;
//     } else {
//         res.render('C',{privateCarsSell:privateCarsSell});
//         };
//         });
//         });


app.get(['/DM/C'],function(req,res){
    var privateCarSql = 'SELECT carId, carName, carStatus FROM privateCar where currentUserId= ? and carStatus = "판매중"';
        conn.query(privateCarSql,[req.session.userId], function(err,privateCars,fields){
    var privateCarSellSql = 'SELECT carId, carName, carStatus FROM privateCar where currentUserId= ? and carStatus = "판매완료"';
        conn.query(privateCarSellSql,[req.session.userId], function(err,privateCarsSell,fields){
    if(err){
        console.log("DB Error Occurred");
        return;
    } else {
        res.render('C',{privateCars:privateCars,privateCarsSell:privateCarsSell});
        };
        });
    });
});





/* POST 호출 처리 */
app.post('/DM/D', function(req, res){

	res.send(req.body);
});


app.get('/DM/D', function(req, res){
 res.render('D');
});



app.get('/DM/layA', function(req, res){
 res.render('layA');
});


app.get('/DM/layB', function(req, res){
 res.render('layB');
});





//layB에서의 allCarList에 yes로 되어있어 모든 이가 볼 수 있는 allCarList 페이지 




app.get('/DM/layBallCarList',function(req,res){
     var page = req.query.page;
     var perPage = req.query.perPage;
     var nowPage = (page-0);
     var nextPage = nowPage+5;
     var prevPage = nowPage-5;
     var privateCarSql = 'select * from privateCar where allCarList= ? order by carId limit 5 offset ?';
     conn.query(privateCarSql,['yes',nowPage], function(err,privateCars,fields){
    if(err){
        console.log("DB Error Occurred");
        return;
    } else {
        res.render('layBallCarList',{privateCars:privateCars, nowPage:nowPage, nextPage:nextPage, prevPage:prevPage });
        }
        });
        });







//layB에서의 allCarListSearch 검색창 (yes한것만 나와야함 위와 연결 필수)

app.get(['/DM/layBallCarListSearch'],function(req,res){
    var carName = req.query.carName;
    var searchSql = "SELECT * FROM privateCar WHERE carName like '%" + carName + "%'";
        conn.query(searchSql, function(err,privateCars,fields){
    if(err){
        console.log("DB Error Occurred");
        return;
    } else {
        res.render('allCarListSearch',{privateCars:privateCars});
        };
        });
        });









//layB에서의 찜한 차 카트넣는 페이지 allCarListCartAdd     allCarListCartAdd.jade  submit누르면 DB에는 저장되는데 페이지이동이 안됨
app.post(['/DM/allCarList/:carId/cartAdd'],function(req,res){
    var carId = req.params.carId;
    var myCartSql = 'SELECT carId FROM myCart where currentUserId = ?';
        conn.query(myCartSql,[req.session.userId] ,function(err,myCarts,fields){
    if(carId){
    var myCartSql = 'INSERT INTO myCart(currentUserId,carId) VALUES(?,?)';
        conn.query(myCartSql, [req.session.userId, carId], function(err, myCart, fields){
    if(err){
        console.log("DB Error Occurred");
    } else {
        res.redirect('/DM/allCarList');
        return;
        };
        });
        }
        });
        });


app.get(['/DM/allCarList/:carId/cartAdd'],function(req,res){
    var carId = req.params.carId;
    var myCartSql = 'SELECT carId FROM privateCar where carId = ?';
        conn.query(myCartSql,[carId], function(err,myCart,fields){
    if(err){
        console.log("DB Error Occurred");
        return;
    } else {
        res.render('allCarListCartAdd',{myCart:myCart[0]});
        return;
        };
        });   
        });
 

//layB에서의 allCarList에서 상세보기 누르면 나오는 페이지 allCarListInfo
app.get(['/DM/layBallCarList/:carId/allCarListInfo'],function(req,res){
    var carId = req.params.carId;
    var allCarListSql = 'SELECT * FROM privateCar WHERE carId = ?';
        conn.query(allCarListSql,[carId],function(err,allCarList,fields){
    if(carId){
    var myInfoSql = 'SELECT privateCar.currentUserId, users1.* FROM privateCar INNER JOIN users1 ON privateCar.currentuserId = users1.id where carId = ?';
        conn.query(myInfoSql, [carId], function(err, myInfo, fields){        
    if(err){
        console.log("DB Error Occurred");
        return;
    } else {
        res.render('layBallCarListInfo',{myInfo:myInfo[0], allCarList:allCarList[0]});
        };
        });
        };
        });
});





app.get('/DM/benz',function(req,res){
    fs.readfile('benz.png',function(error,data){
    res.end(data);
});
});




app.get('/DM/layBnaverLogin', function(req, res){
 res.render('layBnaverLogin');
});





app.listen(3001, function(){
    console.log('3001 OPEN');
    });


